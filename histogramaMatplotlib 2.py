#Muestra el histograma de una imagen mediante matplotlib

import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

#lee la imagen y la guarda en una variable
img = cv.imread('../Externo/foto.jpg',0)

#muestra la imagen original
cv.imshow('HISTOGRAMA',img)

#muestra el histograma de la imagen mediante matplotlib
plt.hist(img.ravel(),256,[0,256]); plt.show()

