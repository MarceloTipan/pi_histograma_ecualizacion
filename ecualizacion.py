#Ecualiza una imagen mediante opencv
#Imprime una imagen y su histograma mediante matplotlib

import cv2 as cv
from matplotlib import pyplot as plt
import matplotlib.image as mpimg

#lee la imagen y la guarda en 'img'
img = cv.imread('../Externo/calle.jpg')

#Separa la imagen en los tres canales
b,g,r = cv.split(img)
#Reordena los canales para imprimir en matplotlib
rgb_img = cv.merge([r,g,b])

#transforma la imagen a formato yuv para poder calcular el histograma
#se usa BGR ya que es la forma en que opencv lee la imagen
img_to_yuv = cv.cvtColor(img ,cv.COLOR_BGR2YUV)

#se ecualiza la imagen
img_to_yuv[: ,: ,0] = cv.equalizeHist(img_to_yuv[: ,: ,0])

#se vuelve a transformar la imagen a rgb
hist_equalization_result = cv.cvtColor(img_to_yuv, cv.COLOR_YUV2BGR)
#Separa la imagen en los tres canales
b,g,r = cv.split(hist_equalization_result)
#Reordena los canales para imprimir en matplotlib
rgb_resul = cv.merge([r,g,b])

#define los colores
color = ('b','g','r')
#genera una ventana
plt.subplot(221), plt.imshow(rgb_img, 'gray')
plt.subplot(222), plt.imshow(rgb_resul,'gray')
plt.subplot(223), \
#genera el histograma de r,g,b
for i,col in enumerate(color):
    histr = cv.calcHist([img],[i],None,[256],[0,256])
    plt.plot(histr,color = col)

#plt.plot(hist_full)
plt.subplot(224), \
#genera el histograma de r,g,b
for i,col in enumerate(color):
    histr = cv.calcHist([hist_equalization_result],[i],None,[256],[0,256])
    plt.plot(histr,color = col)

#plt.plot(hist_mask)
plt.xlim([0,256])
plt.show()



