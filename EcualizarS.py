import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

img = cv.imread('../Externo/Lenna2.png')
b,g,r = cv.split(img)       # get b,g,r
rgb_img = cv.merge([r,g,b])

img_to_yuv = cv.cvtColor(img, cv.COLOR_BGR2YUV)
img_to_yuv[:, :, 0] = cv.equalizeHist(img_to_yuv[:, :, 0])
hist_equalization_result = cv.cvtColor(img_to_yuv, cv.COLOR_YUV2BGR)

#cv.imwrite('./imagenes/resultimagen4.jpg', hist_equalization_result)
#resul=cv.imread('./imagenes/resultimagen4.jpg')
b,g,r = cv.split(hist_equalization_result)
rgb_resul = cv.merge([r,g,b])


plt.subplot(221), plt.imshow(rgb_img, 'gray')
plt.subplot(222), plt.imshow(rgb_resul,'gray')

color = ('b','g','r')
for i,col in enumerate(color):
    histr = cv.calcHist([img],[i],None,[256],[0,256])
    plt.subplot(223),plt.plot(histr,color = col)
    histrecualizada = cv.calcHist([hist_equalization_result], [i], None, [256], [0, 256])
    plt.subplot(224), plt.plot(histrecualizada, color=col)

plt.xlim([0,256])
plt.show()