#Muestra el histograma de una imagen sobre la cual se aplica una máscara
#Usa la librería Opencv

import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

#lee y guarda la imagen en img
img = cv.imread('../Externo/lapiz.jpg',0)

# create a mask
mask = np.zeros(img.shape[:2], np.uint8)
#define el tamaño de la máscara
mask[0:1000, 0:1000] = 255
#aplica la máscara sobre la imagen
masked_img = cv.bitwise_and(img,img,mask = mask)

# Calculate histogram with mask and without mask
# Check third argument for mask
hist_full = cv.calcHist([img],[0],None,[256],[0,256])
hist_mask = cv.calcHist([img],[0],mask,[256],[0,256])

#genera una ventana para mostrar los resultados
plt.subplot(221), plt.imshow(img, 'gray')
plt.subplot(222), plt.imshow(mask,'gray')
plt.subplot(223), plt.imshow(masked_img, 'gray')
plt.subplot(224), plt.plot(hist_full), plt.plot(hist_mask)
plt.xlim([0,256])
plt.show()