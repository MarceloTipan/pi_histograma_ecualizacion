#Muestra el histograma de una imagen en rgb

import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
import matplotlib.image as mpimg

#lee la imagen y la fuarda en img
#lee con open cv
#img = cv.imread('../Externo/lapiz.jpg')

#lee con matplotlib
img=mpimg.imread('../Externo/calle.jpg')

#define los colores
color = ('b','g','r')

#genera el histograma de r,g,b
for i,col in enumerate(color):
    histr = cv.calcHist([img],[i],None,[256],[0,256])
    plt.plot(histr,color = col)
    plt.xlim([0,256])
plt.show()

imgplot = plt.imshow(img)
plt.show()